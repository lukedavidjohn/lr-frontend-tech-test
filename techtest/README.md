# Restaurant data front-end tech test

A basic front-end web-app built in React to display and filter restaurant information from JSON.

## Getting Started

These instructions will get you a copy of the app repository up and running on your local machine.

### Prerequisites

Node.js

### Setup

To test if Node.js is installed on your machine enter the following command in your command line:

```
npm -v
```

This should return a version number; if you still need to install Node.js please follow the instructions here.

Once you have Node.js set up on your machine, you will need to clone this repository to a local directory.
On the command line navigate to the parent directory where you would like to store the repository and run the following command:

```
git clone https://lukedavidjohn@bitbucket.org/luke-rushworth/frontend-tech-test.git
```

Navigate into the directory on the command line then run the following command to install all of the dependencies from the package.json:

```
npm install
```

Once you have everything installed you can run the following command to open a locally-hosted version of the website where changes to the code will be automatically reflected upon saving:

```
npm start
```

## Author

Luke Rushworth
