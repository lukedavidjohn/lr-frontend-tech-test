import React, { Component } from "react";
import Select from "react-select";
import "./App.css";
const data = require("./restaurants.json");
const uniq = require("lodash.uniq");

class App extends Component {
  state = {
    restaurants: data,
    options: [],
    selectedCuisine: null,
    dogFriendly: false,
    veganOptions: false
  };
  render() {
    const {
      restaurants,
      options,
      selectedCuisine,
      dogFriendly,
      veganOptions
    } = this.state;
    return (
      <div className="App">
        <ul>
          {selectedCuisine
            ? restaurants
                .filter(ele => {
                  // add dogFriendly and veganOptions into filter options here
                  ele.cuisine.includes(selectedCuisine.value);
                })
                .map((ele, idx) => {
                  return (
                    <div key={idx}>
                      <li>
                        Restaurant name: {ele.name}; address: {ele.address};
                        cuisine: {ele.cuisine}; dog-friendly:{" "}
                        {JSON.stringify(ele["dog-friendly"])}; vegan options:{" "}
                        {JSON.stringify(ele["vegan-options"])}
                      </li>
                    </div>
                  );
                })
            : restaurants.map((ele, idx) => {
                return (
                  <div key={idx}>
                    <li>
                      Restaurant name: {ele.name}; address: {ele.address};
                      cuisine: {ele.cuisine}; dog-friendly:{" "}
                      {JSON.stringify(ele["dog-friendly"])}; vegan options:{" "}
                      {JSON.stringify(ele["vegan-options"])}
                    </li>
                  </div>
                );
              })}
        </ul>
        <Select
          value={selectedCuisine}
          onChange={this.handleChange}
          options={options}
        />
        <button value="dog-friendly" onClick={this.handleClick}>
          Show only dog-friendly restaurants
        </button>
        <button value="vegan-options" onClick={this.handleClick}>
          Show only restaurants with vegan options
        </button>
      </div>
      // Add "clear all filter options" button here - functions by setting state of dogFriendly and veganOptions to false and selectedCuisine to null
    );
  }

  componentDidMount = () => {
    const { restaurants } = this.state;
    const cuisineArray = restaurants.reduce((acc, ele) => {
      acc.push(...ele.cuisine);
      return acc;
    }, []);
    this.setState({
      options: uniq(cuisineArray)
        .sort()
        .map(ele => {
          return { value: ele, label: ele };
        })
    });
  };

  handleChange = selectedCuisine => {
    this.setState({ selectedCuisine });
  };

  handleClick = event => {
    const { value } = event.target;
    if (value === "dog-friendly") {
      this.setState({ dogFriendly: true });
    }
    this.setState({ veganOptions: true });
  };
}

export default App;
